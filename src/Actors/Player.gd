extends Actor

export var stomp_impulse: = 1000.0

func _on_EnemyDetector_area_entered(area):
	velocity = calculate_stomp_velocity(velocity, stomp_impulse)

func _on_EnemyDetector_body_entered(body):
	#queue_free()
	global.lives -= 1
	global.coins = 0
	if (global.lives == 0):
		get_tree().change_scene("res://src/Levels/Game Over.tscn")
	else:
		get_tree().reload_current_scene()

	
	
func _physics_process(delta):
	var is_jump_interrupted: = Input.is_action_just_released("jump") and velocity.y < 0.0
	var direction: = get_direction()
	velocity = calculate_move_velocity(velocity, direction, speed, is_jump_interrupted)
	velocity = move_and_slide(velocity, FLOOR_NORMAL)
	
func get_direction() -> Vector2:
	return Vector2(
		Input.get_action_strength("move_right") - Input.get_action_strength("move_left"),
		#-1.0 if Input.get_action_strength("jump") and is_on_floor() else 1.0
		-Input.get_action_strength("jump") if is_on_floor() and Input.is_action_just_pressed("jump") else 0.0
	)

func calculate_move_velocity(linier_velocity: Vector2, direction: Vector2, speed: Vector2, is_jump_interrupted: bool) -> Vector2:
	var new_velocity: = linier_velocity
	new_velocity.x = speed.x * direction.x
	new_velocity.y += gravity * get_physics_process_delta_time() # to make the chacracter move down (fall)
	if direction.y == -1.0:
		new_velocity.y = speed.y * direction.y
	if is_jump_interrupted:
		new_velocity.y = 0.0
	return new_velocity

func calculate_stomp_velocity(linier_velocity: Vector2, impulse: float) -> Vector2:
	var out: = linier_velocity
	out.y = -impulse
	return out





